import React, {useState} from 'react'
import { useHistory, useLocation } from 'react-router-dom'

import {
  CSubheader,
  CButton,
  CCol,
} from '@coreui/react'

import CIcon from '@coreui/icons-react'
import Modal from '../containers/Modal'
import Toast from '../containers/Toast'

const ActionBar = () => {
  const history = useHistory()
let location = useLocation();
const path = location.pathname;
const [toasts, setToasts] = useState([])
const [view, setView] = useState(false)

const ToggleModal = () => setView(!view);

const addToast = (message) => {
  setToasts([
    ...toasts, 
    { position:'top-right', autohide: true && 5000, closeButton:true, fade:true, title:message, context:"Por favor intente nuevamente." }
  ])
}

  return (<>
      <Toast toasts={toasts} />
      <Modal show={view} close={ToggleModal} message={"¿Quieres eliminar este registro?"}/>
      <CSubheader className={`px-3 justify-content-between action-bar ${(path.substr(0, 7) === '/lists/' || path.substr(0, 7) === '/users/') && "view"}`}>
          <CCol lg={2} className="mfe-2 container">
            <CButton size="sm" variant="outline" color="danger" onClick={()=>{history.push(`${path.substr(0,7) === '/lists/'?"/lists":"/users"}`)}}>
              <CIcon name="cil-arrow-circle-left" />
                Volver
            </CButton>
          </CCol>
          <CCol lg={10} className="mfe-2 container">
            <CButton size="sm" variant="outline" color="info" onClick={() => addToast("Error al generar el PDF")}>
              <CIcon name="cil-file" />
                Generar PDF
            </CButton>
            <CButton size="sm" variant="outline" color="danger" onClick={()=>setView(!view)}>
              <CIcon name="cil-trash"/>
                Eliminar
            </CButton>
            <CButton size="sm" variant="outline" color="primary" onClick={() => addToast("Error al enviar por correo")}>
              <CIcon name="cil-send" />
                Enviar por correo
            </CButton>
          </CCol>
      </CSubheader>
      </>
  )
}

export default ActionBar
