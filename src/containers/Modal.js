import React, {useState} from 'react'

import {
  CButton,
  CModal,
  CModalBody,
  CModalFooter,
  CImg,
  CRow
} from '@coreui/react'

import deleteReg from '../assets/img/trash.png'
import Toast from './Toast'

const Modal = ({show, close, message}) => {
  const [toasts, setToasts] = useState([])
  const addToast = () => {
    setToasts([
      ...toasts, 
      { position:'top-right', autohide: true && 5000, closeButton:true, fade:true, title:"Error al eliminar el registro", context:"Por favor intente nuevamente." }
    ]);
    close()
  }

return (<>
      <Toast toasts={toasts}/>
      <CModal 
        show={show} 
        onClose={close}
      >
        <CModalBody>
          <div className="shape">
            <CImg src={deleteReg} className="delete-reg" alt="item-del"/>
          </div>
            <CRow className="d-flex justify-content-center mt-4">
              <h4>{message}</h4>
            </CRow>
            <CRow className="d-flex justify-content-center">
              <p>No será posible restablecer esta acción</p>
            </CRow>                 
        </CModalBody>
        <CModalFooter>
          <CButton color="danger" onClick={addToast}>Eliminar</CButton>{' '}
          <CButton 
            variant="outline" color="dark"
            onClick={() => close()}
          >Cancelar</CButton>
        </CModalFooter>
      </CModal>
      </>
  )
}

export default Modal
