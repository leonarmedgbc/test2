import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import {
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CPagination
} from '@coreui/react'

import usersData from './UsersData'
import Fields from './Fields'

const Users = () => {
  const noItemsView = { noResults: 'No se encontraron coincidencias', noItems: 'No hay items disponibles' }
  const tableFilter = { placeholder: "Nombre, fecha, role", label: "Filtro:" }
  const itemsPerPageSelect = {label: "Items por Pág.:"}
  const history = useHistory()
  const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
  const [page, setPage] = useState(currentPage)

  const pageChange = newPage => {
    currentPage !== newPage && history.push(`/users?page=${newPage}`)
  }

  useEffect(() => {
    currentPage !== page && setPage(currentPage)
  }, [currentPage, page])

  return (
    <CRow>
      <CCol xl={12}>
        <CCard>
          <CCardHeader>
            Usuarios
            <small className="text-muted"> ejemplo</small>
          </CCardHeader>
          <CCardBody>
          <CDataTable
            items={usersData}
            fields={Fields}
            noItemsView={noItemsView}
            tableFilter={tableFilter}
            hover
            striped
            sorter
            itemsPerPageSelect = {itemsPerPageSelect}
            itemsPerPage={5}
            activePage={page}
            clickableRows
            onRowClick={(item) => history.push(`/users/${item.id}`)}
          />
          <CPagination
            activePage={page}
            onActivePageChange={pageChange}
            pages={5}
            doubleArrows={false} 
            align="center"
          />
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default Users
