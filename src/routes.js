import React from 'react';

const BasicForms = React.lazy(() => import('./views/base/forms/BasicForms'));
const Dashboard = React.lazy(() => import('./views/dashboard/Dashboard'));
const Users = React.lazy(() => import('./views/users/Users'));
const User = React.lazy(() => import('./views/users/User'));
const Lists = React.lazy(() => import('./views/lists/Lists'));
const List = React.lazy(() => import('./views/lists/List'));

const routes = [
  { path: '/', exact: true, name: 'Home' },
  { path: '/dashboard', name: 'Dashboard', component: Dashboard },
  { path: '/users', exact: true,  name: 'Users', component: Users },
  { path: '/users/:id', exact: true, name: 'User Details', component: User },
  { path: '/lists', exact: true,  name: 'Items', component: Lists },
  { path: '/lists/:id', exact: true, name: 'Items Details', component: List },
  { path: '/base/forms', name: 'Forms', component: BasicForms },
];

export default routes;
