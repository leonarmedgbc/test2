const Fields = [
  { key: 'nombre', _style: { width: '40%'} },
  'fecha',
  { key: 'status', _style: { width: '20%'} },
  {
    key: 'acción',
    label: '',
    _style: { width: '1%' },
    sorter: false,
    filter: false
  }
]

export default Fields
