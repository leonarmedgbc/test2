import React, { useState, useEffect } from 'react'
import { useHistory, useLocation } from 'react-router-dom'
import {
  CBadge,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CDataTable,
  CRow,
  CPagination,
  CButton,
  CCollapse,
} from '@coreui/react'

import Modal from '../../containers/Modal'
import DataList from './DataList'
import Fields from './Fields'

const getBadge = status => {
  switch (status) {
    case 'Active': return 'success'
    case 'Inactive': return 'secondary'
    case 'Pending': return 'warning'
    case 'Banned': return 'danger'
    default: return 'primary'
  }
}

const Data = () => {
  const history = useHistory()
  const queryPage = useLocation().search.match(/page=([0-9]+)/, '')
  const currentPage = Number(queryPage && queryPage[1] ? queryPage[1] : 1)
  const [page, setPage] = useState(currentPage)
  const noItemsView = { noResults: 'No se encontraron coincidencias', noItems: 'No hay items disponibles' }
  const tableFilter = { placeholder: "Nombre, fecha, status", label: "Filtro:" }
  const itemsPerPageSelect = {label: "Items por Pág.:"}
  const [view, setView] = useState(false)
  const [details, setDetails] = useState([])

  const pageChange = newPage => {
    currentPage !== newPage && history.push(`/lists?page=${newPage}`)
  }

  const toggleDetails = (index) => {
    const position = details.indexOf(index)
    let newDetails = details.slice()
    if (position !== -1) {
      newDetails.splice(position, 1)
    } else {
      newDetails = [...details, index]
    }
    setDetails(newDetails)
  }

  useEffect(() => {
    currentPage !== page && setPage(currentPage)
  }, [currentPage, page])
  
  const ToggleModal = () => setView(!view);

  return (<>
    <Modal show={view} close={ToggleModal} message={"¿Quieres eliminar este registro?"} />
    <CRow className="list-products">
      <CCol xl={12}>
        <CCard>
          <CCardHeader>
            Listado de Items
            <small className="text-muted"> Data de prueba</small>
          </CCardHeader>
          <CCardBody>
          <CDataTable
            items={DataList}
            fields={Fields}
            noItemsView={noItemsView}
            tableFilter={tableFilter}
            hover
            striped
            sorter
            itemsPerPageSelect = {itemsPerPageSelect}
            itemsPerPage={5}
            activePage={page}
            scopedSlots = {{
              'status':
                (item)=>(
                  <td>
                    <CBadge color={getBadge(item.status)}>
                      {item.status}
                    </CBadge>
                  </td>
                ),
              'acción':
                (item, index)=>{
                  return (
                    <td className="py-2">
                      <CButton
                        color="primary"
                        variant="outline"
                        shape="square"
                        size="sm"
                        onClick={()=>{toggleDetails(index)}}
                      >
                        {details.includes(item,index) ? 'Ocultar' : 'Ver'}
                      </CButton>
                    </td>
                  )
                },
              'details':
                (item, index)=>{
                  return (
                  <CCollapse show={details.includes(index)}>
                    <CCardBody>
                      <p className="text-muted">Registrado el: {item.fecha}</p>
                      <CButton size="sm" color="primary" onClick={() => history.push(`/lists/${index}`)}>
                        Reporte
                      </CButton>
                      <CButton size="sm" color="danger" className="ml-1" onClick={()=>setView(!view)}>
                        Eliminar
                      </CButton>
                    </CCardBody>
                  </CCollapse>
                )
              }
            }}    
          />
          {DataList.length>=6 && <CPagination
            activePage={page}
            onActivePageChange={pageChange}
            pages={5}
            doubleArrows={false} 
            align="center"
          />}
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  </>)
}

export default Data
