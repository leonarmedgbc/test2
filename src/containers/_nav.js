import React from 'react'
import CIcon from '@coreui/icons-react'

const _nav =  [
  {
    _tag: 'CSidebarNavItem',
    name: 'Dashboard',
    to: '/dashboard',
    icon: <CIcon name="cil-speedometer" customClasses="c-sidebar-nav-icon"/>,
    badge: {
      color: 'info',
      text: 'NEW',
    }
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['GBC']
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Listado',
    to: '/lists',
    icon: 'cil-list-filter',
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Formulario',
    to: '/base/forms',
    icon: 'cib-server-fault',
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Proyectos']
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Proyecto 1',
    route: '/base',
    icon: 'cil-puzzle',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Fase 1',
        to: '/base/breadcrumbs',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Fase 2',
        to: '/base/cards',
      }
    ],
  },
  {
    _tag: 'CSidebarNavDropdown',
    name: 'Proyecto 2',
    route: '/buttons',
    icon: 'cil-cursor',
    _children: [
      {
        _tag: 'CSidebarNavItem',
        name: 'Fase 1',
        to: '/buttons/buttons',
      },
      {
        _tag: 'CSidebarNavItem',
        name: 'Fase 2',
        to: '/buttons/brand-buttons',
      }
    ],
  },
  {
    _tag: 'CSidebarNavDivider'
  },
  {
    _tag: 'CSidebarNavTitle',
    _children: ['Configuraciones'],
  },
  {
    _tag: 'CSidebarNavItem',
    name: 'Usuarios',
    to: '/users',
    icon: 'cil-user'
  },
  {
    _tag: 'CSidebarNavDivider',
    className: 'm-2'
  },
  {
    _tag: 'CSidebarNavDivider',
    className: 'm-2'
  }
]

export default _nav
