const usersData = [
  {id: 0, nombre: 'Item 1', fecha: '2018/01/01', status: 'Pending'},
  {id: 1, nombre: 'Item 2', fecha: '2018/01/01', status: 'Active'},
  {id: 2, nombre: 'Item 3', fecha: '2018/02/01', status: 'Banned'},
  {id: 3, nombre: 'Item 4', fecha: '2018/02/01', status: 'Inactive'},
  {id: 4, nombre: 'Item 5', fecha: '2018/03/01', status: 'Pending'}
]

export default usersData
