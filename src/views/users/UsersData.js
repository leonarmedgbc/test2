const usersData = [
  {id: 0, nombre: 'John Doe', registrado: '2018/01/01', role: 'Guest', status: 'Pending'},
  {id: 1, nombre: 'Samppa Nori', registrado: '2018/01/01', role: 'Member', status: 'Active'},
  {id: 2, nombre: 'Estavan Lykos', registrado: '2018/02/01', role: 'Staff', status: 'Banned'},
  {id: 3, nombre: 'Chetan Mohamed', registrado: '2018/02/01', role: 'Admin', status: 'Inactive'},
  {id: 4, nombre: 'Derick Maximinus', registrado: '2018/03/01', role: 'Member', status: 'Pending'},
  {id: 5, nombre: 'Friderik Dávid', registrado: '2018/01/21', role: 'Staff', status: 'Active'},
  {id: 6, nombre: 'Yiorgos Avraamu', registrado: '2018/01/01', role: 'Member', status: 'Active'},
  {id: 7, nombre: 'Avram Tarasios', registrado: '2018/02/01', role: 'Staff', status: 'Banned'},
  {id: 8, nombre: 'Quintin Ed', registrado: '2018/02/01', role: 'Admin', status: 'Inactive'},
  {id: 9, nombre: 'Enéas Kwadwo', registrado: '2018/03/01', role: 'Member', status: 'Pending'},
  {id: 10, nombre: 'Agapetus Tadeáš', registrado: '2018/01/21', role: 'Staff', status: 'Active'},
  {id: 11, nombre: 'Carwyn Fachtna', registrado: '2018/01/01', role: 'Member', status: 'Active'},
  {id: 12, nombre: 'Nehemiah Tatius', registrado: '2018/02/01', role: 'Staff', status: 'Banned'},
  {id: 13, nombre: 'Ebbe Gemariah', registrado: '2018/02/01', role: 'Admin', status: 'Inactive'},
  {id: 14, nombre: 'Eustorgios Amulius', registrado: '2018/03/01', role: 'Member', status: 'Pending'},
  {id: 15, nombre: 'Leopold Gáspár', registrado: '2018/01/21', role: 'Staff', status: 'Active'},
  {id: 16, nombre: 'Pompeius René', registrado: '2018/01/01', role: 'Member', status: 'Active'},
  {id: 17, nombre: 'Paĉjo Jadon', registrado: '2018/02/01', role: 'Staff', status: 'Banned'},
  {id: 18, nombre: 'Micheal Mercurius', registrado: '2018/02/01', role: 'Admin', status: 'Inactive'},
  {id: 19, nombre: 'Ganesha Dubhghall', registrado: '2018/03/01', role: 'Member', status: 'Pending'},
  {id: 20, nombre: 'Hiroto Šimun', registrado: '2018/01/21', role: 'Staff', status: 'Active'},
  {id: 21, nombre: 'Vishnu Serghei', registrado: '2018/01/01', role: 'Member', status: 'Active'},
  {id: 22, nombre: 'Zbyněk Phoibos', registrado: '2018/02/01', role: 'Staff', status: 'Banned'},
  {id: 23, nombre: 'Aulus Agmundr', registrado: '2018/01/01', role: 'Member', status: 'Pending'},
  {id: 42, nombre: 'Ford Prefect', registrado: '2001/05/25', role: 'Alien', status: 'Don\'t panic!'}
]

export default usersData
