import React from 'react'
import { CCard, CCardBody, CCardHeader, CCol, CRow } from '@coreui/react'
import CIcon from '@coreui/icons-react'

import ActionBar from '../../containers/ActionBar'
import dataList from './DataList'

const List = ({match}) => {
  const item = dataList.find( item => item.id.toString() === match.params.id)
  const dataDetails = item ? Object.entries(item) : 
    [['id', (<span><CIcon className="text-muted" name="cui-icon-ban" /> No hay registros</span>)]]

  return (<>
    <CRow>
      <CCol lg={12}>
        <ActionBar />
      </CCol>
      <CCol lg={6}>
        <CCard>
          <CCardHeader>
            Registro id: {match.params.id}
          </CCardHeader>
          <CCardBody>
              <table className="table table-striped table-hover">
                <tbody>
                  {
                    dataDetails.map(([key, value], index) => {
                      return (
                        <tr key={index.toString()}>
                          <td>{`${key}:`}</td>
                          <td><strong>{value}</strong></td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
          </CCardBody>
        </CCard>
      </CCol>
      <CCol lg={6}>
        <CCard>
          <CCardHeader>
            Registro id: {match.params.id}
          </CCardHeader>
          <CCardBody>
              <table className="table table-striped table-hover">
                <tbody>
                  {
                    dataDetails.map(([key, value], index) => {
                      return (
                        <tr key={index.toString()}>
                          <td>{`${key}:`}</td>
                          <td><strong>{value}</strong></td>
                        </tr>
                      )
                    })
                  }
                </tbody>
              </table>
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  </>)
}

export default List
